(ns camelot.launcher.renderer.view.util.core)

(def modifier-key? #{"Alt" "AltGraph" "CapsLock" "Control" "Fn" "Hyper" "Meta"
                     "NumLock" "OS" "Scroll" "ScrollLock" "Shift" "Super"
                     "Symbol" "SymbolLock" "Win"})
