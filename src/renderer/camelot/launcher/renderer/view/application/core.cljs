(ns camelot.launcher.renderer.view.application.core
  (:require
   [camelot.launcher.renderer.subs.config :as subs]
   [re-frame.core :as re-frame]
   [reagent.core :as reagent]))

(defn to-main
  [route]
  (re-frame/dispatch [:navigation-route-change-event
                      {:next-route [:main]}]))

(defn webview
  []
  (let [http-port @(re-frame/subscribe [::subs/server-http-port])]
    [:div {:className "section"}
     [:iframe {:src (str "http://localhost:" http-port)
               :className "full-page"}]
     [:div {:className "back-to-launcher"}
      [:a {:href "#"
           :onClick to-main
           :title "Back to launcher"
           :className "subtle-link"}
       "←"]]]))
