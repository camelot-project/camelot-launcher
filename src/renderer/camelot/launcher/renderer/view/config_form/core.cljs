(ns camelot.launcher.renderer.view.config-form.core
  (:require
   [re-frame.core :as re-frame]
   [camelot.launcher.renderer.subs.config :as subs]))

(defn on-change-dispatcher
  [{:keys [:config/path :config/type :config/validator :config/transformer]
    :or {transformer identity}}]
  #(let [raw-value (if (= (.. % -target -type) "checkbox")
                     (.. % -target -checked)
                     (.. % -target -value))
         nv (condp = type
              :config/number
              (js/parseInt raw-value 10)

              :config/float
              (js/parseFloat raw-value)

              :config/directory-chooser
              (.-path (aget (.. % -target -files) 0))

              raw-value)
         p {:path path
            :next-value (transformer nv)}]
     (re-frame/dispatch [:config-value-change-event p])))

(defn- text-opt
  [{:keys [value dispatcher]}]
  [:input {:className "field-input"
           :on-change dispatcher
           :value value
           :type "text"}])

(defn- password-opt
  [{:keys [value dispatcher]}]
  [:input {:className "field-input"
           :on-change dispatcher
           :value value
           :type "password"}])

(defn- checkbox-opt
  [{:keys [value dispatcher]}]
  [:input {:className "field-input-checkbox"
           :on-change dispatcher
           :checked (or (= value true) (= value "on"))
           :type "checkbox"}])

(defn- directory-chooser-opt
  [{:keys [value dispatcher]}]
  [:div
   [:pre {:className "subtle"} value]
   [:input {:className "field-input"
            :on-change dispatcher
            :type "file"
            :webkitdirectory "true"}]])

(defn- number-opt
  [{:keys [config/props]} {:keys [value dispatcher]}]
  [:input (merge {:className "field-input"
                  :on-change dispatcher
                  :value value
                  :type "number"
                  :webkitdirectory "true"}
                 props)])

(defn- select-opt
  [{:keys [value dispatcher :config/options]}]
  [:select {:className "field-input"
            :on-change dispatcher
            :value value}
   (doall (for [[value label] options]
            [:option {:key value :value value} label]))])

(defn- show-field?
  [opt]
  (or
   (nil? (:config/condition opt))
   ((:config/condition opt) @(re-frame/subscribe [::subs/config]))))

(defn- clear-action
  [{:keys [dispatcher]}]
  [:button {:className "dataset-remove btn"
            :on-click (dispatcher :config-value-clear-event)}
   "Clear"])

(defn- extra-actions-opt
  [actions props]
  [:div
   (doall (for [action actions]
            (condp = action
              :action/clear
              [clear-action props])))])

(defn- render-option
  [_ opt]
  (let [path (:config/path opt)
        value (or @(re-frame/subscribe [::subs/config path]) "")
        problem? @(re-frame/subscribe [::subs/problem? path])
        dispatcher (on-change-dispatcher opt)
        props (merge opt {:value value :dispatcher dispatcher})]
    (when (show-field? opt)
      [:div {:className "field-container"}
       [:label {:className "field-label"}
        (:config/label opt)
        (when-let [actions (seq (:config/title-actions opt))]
            [extra-actions-opt actions
             {:dispatcher (fn [action]
                            (fn [_] (re-frame/dispatch [action {:path path}])))}])]
       (if-let [element-type (:element/type opt)]
         (condp = element-type
           :separator
           [:hr]

           (if-let [component (get-in opt [:element-type-components element-type])]
             [component]
             (println "Component not found")))
         (condp = (:config/type opt)
           :config/directory-chooser
           [directory-chooser-opt props]

           :config/text
           [text-opt props]

           :config/password
           [password-opt props]

           :config/number
           [number-opt opt props]

           :config/float
           [number-opt opt props]

           :config/checkbox
           [checkbox-opt props]

           :config/select
           [select-opt props]))
       (when problem?
         [:label {:className "validation-error"}
          (or (:config/problem-message opt)
              "Please check this setting and try again")])])))

(defn- option-key
  [opt]
  (apply str (map name (:config/path opt))))

(defn form
  [{:keys [options element-type-components]}]
  [:div {:className "scroll-container"}
   [:div {:className "field-input-form"}
    (doall (for [opt options]
             [render-option {:key (option-key opt)} (assoc opt :element-type-components element-type-components)]))]])
