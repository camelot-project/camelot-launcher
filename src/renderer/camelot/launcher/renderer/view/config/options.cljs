(ns camelot.launcher.renderer.view.config.options)

(def ^:private path-requirement
  "Make sure the location exists and that you have permission")

(def config-options
  [{:config/path [:species-name-style]
    :config/label "Species name style"
    :config/type :config/select
    :config/options [[:common-name "Common"]
                     [:scientific "Scientific"]]
    :config/problem-message "Please select a valid species name style"}
   {:config/path [:send-usage-data]
    :config/label "Send anonymous usage data?"
    :config/type :config/checkbox
    :config/problem-message "Please check this on or off"}
   {:element/type :detector/separator}
   {:config/path [:detector :enabled]
    :config/label "Enable wildlife detection?"
    :config/type :config/checkbox}
   {:config/path [:detector :username]
    :config/label "Email address"
    :config/type :config/text
    :config/condition #(-> % :detector :enabled)}
   {:config/path [:detector :password]
    :config/label "API key"
    :config/type :config/password
    :config/condition #(-> % :detector :enabled)}
   {:element/type :detector/check-authentication-button
    :config/condition #(-> % :detector :enabled)}
   {:config/path [:detector :confidence-threshold]
    :config/label "Confidence threshold"
    :config/type :config/float
    :config/condition #(-> % :detector :enabled)
    :config/props {:step 0.01 :min 0 :max 1}}
   {:element/type :advanced-settings-separator}
   {:config/path [:paths :application]
    :config/label "Installation location"
    :config/type :config/directory-chooser
    :config/problem-message path-requirement}
   {:config/path [:paths :backup]
    :config/label "Default backup location"
    :config/type :config/directory-chooser
    :config/problem-message path-requirement}
   {:config/path [:paths :log]
    :config/label "Log file location"
    :config/type :config/directory-chooser
    :config/problem-message path-requirement}
   {:config/path [:paths :root]
    :config/label "Import scanning root location"
    :config/type :config/directory-chooser
    :config/problem-message path-requirement}
   {:config/path [:java-command]
    :config/label "Java command"
    :config/type :config/text
    :config/problem-message "Please set a valid location to your java command"}
   {:config/path [:server :max-heap-size]
    :config/label "JVM maximum heap size (MB)"
    :config/type :config/number
    :config/problem-message "Please set a valid heap size in megabytes"}
   {:config/path [:server :jvm-extra-args]
    :config/label "JVM options"
    :config/type :config/text
    :config/problem-message "Please set valid JVM arguments or leave this blank"}
   {:config/path [:server :http-port]
    :config/label "HTTP port"
    :config/type :config/number
    :config/problem-message "Please use a valid port number"}])
