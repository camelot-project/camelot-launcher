(ns camelot.launcher.renderer.view.datasets.options
  (:require [clojure.string :as str]))

(def ^:private path-requirement
  "Make sure the location exists and that you have permission")

(def database-directory-name "Database")
(def database-suffix-regexp (re-pattern (str "[\\/]" database-directory-name "$")))

(defn transformer
  [value]
  (str/replace value database-suffix-regexp ""))

(defn config-options
  [dataset]
  [{:config/path [:datasets dataset :paths :media]
    :config/label "Media location"
    :config/type :config/directory-chooser
    :config/problem-message path-requirement}
   {:config/path [:datasets dataset :paths :database]
    :config/label "Database location"
    :config/type :config/directory-chooser
    :config/transformer transformer
    :config/problem-message path-requirement}
   {:config/path [:datasets dataset :paths :backup]
    :config/label "Backup location"
    :config/title-actions [:action/clear]
    :config/type :config/directory-chooser
    :config/problem-message path-requirement}
   {:config/path [:datasets dataset :paths :filestore-base]
    :config/label "FileStore location"
    :config/type :config/directory-chooser
    :config/problem-message path-requirement}
   {:element/type :separator}])
