(ns camelot.launcher.renderer.view.datasets.core
  (:require
   [camelot.launcher.renderer.view.config-form.core :as config-form]
   [camelot.launcher.renderer.view.datasets.options :as options]
   [camelot.launcher.renderer.subs.config :as subs]
   [camelot.launcher.renderer.view.components.common :as common-components]
   [camelot.launcher.renderer.view.util.core :as util]
   [clojure.string :as str]
   [re-frame.core :as re-frame]
   [reagent.core :as reagent]))

(defn button-container
  [datasets]
  (let [saving? @(re-frame/subscribe [::subs/config-saving?])]
    [:div {:className "flex-end"}
     [:div {:className "button-container"}
      [:button {:className "btn btn-primary"
                :on-click #(re-frame/dispatch [:config-save-request-event])
                :disabled (or saving? (empty? datasets))}
       "Save"]]]))

(defn- intro
  []
  [:div {:className "section-intro"}
   [:h3 "Datasets"
    [:sup
     [:a {:href "https://camelot-project.readthedocs.io/en/latest/datasets.html"  :target "_blank" :rel "noopener noreferrer"
          :className "learn-more-link"}
      "Learn more"]]]
   [:div {:className "banner warning"}
    [:p
     "Please take the time to read the " [:a {:href "https://camelot-project.readthedocs.io/en/latest/datasets.html#best-practices" :target "_blank" :rel "noopener noreferrer"} "best practice documentation"] " before changing the location of dataset files. Incorrect configuration can result in errors, poor performance and data loss."]]])

(defn- valid-dataset-name?
  [datasets dataset-name]
  (and (re-matches #"(?i)^[-a-z0-9_]+$" dataset-name)
       (nil? (datasets (keyword dataset-name)))))

(defn- add-dataset-handler
  [dataset-name-ref]
  (let [dataset-name @dataset-name-ref
        evt [:dataset-add-event {:dataset-key (keyword (str/lower-case dataset-name))
                                 :dataset-name dataset-name}]]
    (re-frame/dispatch evt))
  (reset! dataset-name-ref ""))

(defn add-dataset
  []
  (let [dataset-name (reagent/atom "")]
    (fn []
      (let [saving? @(re-frame/subscribe [::subs/config-saving?])
            datasets (set @(re-frame/subscribe [::subs/datasets]))
            valid-name? (valid-dataset-name? datasets @dataset-name)]
        [:div {:className "dataset-wrapper"}
         [:div {:className "add-dataset"}
          [:input {:type "text"
                   :placeholder "New dataset name..."
                   :className "dataset-input"
                   :value @dataset-name
                   :on-key-down #(when (and valid-name?
                                            (not (util/modifier-key? (.-key %)))
                                            (= (.-key %) "Enter"))
                                   (add-dataset-handler dataset-name))
                   :on-change #(reset! dataset-name (.. % -target -value))}]
          [:button {:className "btn btn-primary"
                    :on-click #(add-dataset-handler dataset-name)
                    :disabled (or saving?
                                  (empty? @dataset-name)
                                  (not valid-name?))}
           "+"]]
         [:div
          (let [problem? (not (or (empty? @dataset-name) valid-name?))]
            [:div {:className (if problem?  "validation-error" "")}
             (when problem? "This name is invalid or already in use")])]]))))

(defn remove-dataset
  [dataset]
  (let [saving? @(re-frame/subscribe [::subs/config-saving?])]
    [:button {:className "dataset-remove btn"
              :on-click #(re-frame/dispatch
                          [:dataset-remove-event
                           {:dataset-key dataset}])
              :disabled saving?}
     "x"]))

(defn dataset-component
  [dataset]
  (let [dataset-name @(re-frame/subscribe [::subs/dataset-name dataset])]
    [:div {:className "dataset-wrapper"}
     [remove-dataset dataset]
     [:h4 "Dataset: "
      [:input {:value dataset-name
               :className "dataset-input"
               :on-change #(let [v (.. % -target -value)
                                 p {:path [:datasets dataset :name]
                                    :next-value v}]
                             (re-frame/dispatch [:config-value-change-event p]))}]]
     [:div {:className "dataset-form"}
      [config-form/form
       {:options (options/config-options dataset)}]]]))

(defn render-form
  []
  (let [problem? @(re-frame/subscribe [::subs/any-problem?])
        datasets @(re-frame/subscribe [::subs/datasets])]
    [:div {:className "flex-column"}
     [:div {:className (if problem? "validation-error" "")}
      (when problem? "There was a problem with the configuration")]
     (when (empty? datasets)
       [:div {:className "validation-error"}
        "Add at least 1 dataset to save your configuration"])
     (doall (for [dataset datasets]
              [dataset-component dataset]))
     [add-dataset]
     [:br]
     [button-container datasets]]))

(defn datasets-panel
  []
  (re-frame/dispatch [:config-read-request-event])
  (fn []
    [:div {:className "section"}
     (if @(re-frame/subscribe [::subs/config-loaded?])
       [:div
        [intro]
        [render-form]]
       [common-components/spinner])]))
