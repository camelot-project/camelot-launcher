(ns camelot.launcher.renderer.events.fx.logs
  (:require [camelot.launcher.common.events :as common-events]
            [camelot.launcher.renderer.events.ipc :as ipc]
            [re-frame.core :as re-frame]))

(defn logs-read-request-fx
  [_ _]
  (ipc/send :renderer
            (common-events/logs-read-request))
  {})

(defn logs-read-failure-fx
  [_ [_ {:keys [error]}]]
  (js/alert (str (name (:type error)) ": " (:message error)))
  {})

(defn logs-read-success-db-fx
  [db [_ {:keys [contents]}]]
  (assoc db :logs contents))
