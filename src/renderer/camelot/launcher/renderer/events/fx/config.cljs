(ns camelot.launcher.renderer.events.fx.config
  (:require [camelot.launcher.common.events :as common-events]
            [camelot.launcher.renderer.events.ipc :as ipc]
            [camelot.market.config :as market-config]
            [re-frame.core :as re-frame]))

(defn config-read-request-fx
  [_ _]
  (ipc/send :renderer
            (common-events/config-read-request))
  {})

(defn config-read-failure-fx
  [{:keys [db]} [_ {:keys [configuration error problems]}]]
  {:db (-> db
           (assoc-in [:config :problems] (set (map #(map keyword %) problems)))
           (assoc-in [:config :buffer] configuration))})

(defn config-read-fx
  [{:keys [db]} [_ {:keys [configuration]}]]
  {:db (-> db
           (assoc-in [:config :buffer] configuration)
           (assoc-in [:config :datasets]
                     (vec (sort (keys (:datasets configuration))))))})

(defn config-value-change-fx
  [{:keys [db]} [_ {:keys [path next-value]}]]
  {:db (-> db
           (assoc-in `[:config :buffer ~@path] next-value)
           (update-in [:config :problems] disj path))})

(defn- all-but-last
  [coll]
  (let [n (dec (count coll))]
    (take n coll)))

(defn config-value-clear-fx
  [{:keys [db]} [_ {:keys [path]}]]
  {:db (update-in db `[:config :buffer ~@(all-but-last path)]
                  dissoc (last path))})

(defn config-save-request-fx
  [{:keys [db]} _]
  (let [config (-> db :config :buffer)]
    (ipc/send :renderer (common-events/config-save-request config)))
  {:db (assoc-in db [:config :saving?] true)})

(defn config-reset-request-fx
  [{:keys [db]} _]
  (ipc/send :renderer (common-events/config-reset-request))
  {})

(defn config-save-success-fx
  [{:keys [db]} [_ {:keys [configuration]}]]
  {:db (-> db
           (assoc-in [:config :saving?] false)
           (assoc-in [:config :problems] nil))
   :dispatch [:navigation-route-change-event {:next-route [:main]}]})

(defn config-save-failure-fx
  [{:keys [db]} [_ {:keys [configuration problems]}]]
  {:db (-> db
           (assoc-in [:config :buffer] configuration)
           (assoc-in [:config :problems] (set (map #(map keyword %) problems)))
           (assoc-in [:config :saving?] false))})

(defn dataset-add-fx
  [{:keys [db]} [_ {:keys [dataset-key dataset-name]}]]
  {:db (-> db
           (update-in [:config :buffer :datasets dataset-key]
                      #(or % {:paths (market-config/get-default-dataset-paths
                                      (get-in db [:config :buffer :paths])
                                      dataset-key)
                              :name dataset-name}))
           (update-in [:config :datasets] #(vec (distinct (conj % dataset-key)))))})

(defn dataset-remove-fx
  [{:keys [db]} [_ {:keys [dataset-key]}]]
  {:db (-> db
         (update-in [:config :buffer :datasets] dissoc dataset-key)
         (update-in [:config :datasets] #(into [] (remove #{dataset-key} %))))})
