(ns camelot.launcher.renderer.events.core
  (:require [camelot.launcher.common.events :as common-events]
            [camelot.launcher.renderer.events.fx.config :as config-fx]
            [camelot.launcher.renderer.events.fx.process :as process-fx]
            [camelot.launcher.renderer.events.fx.logs :as logs-fx]
            [camelot.launcher.renderer.events.fx.navigation :as navigation-fx]
            [camelot.launcher.renderer.events.ipc :as ipc]
            [re-frame.core :as re-frame]))

(def default-db
  {:navigation
   {:route [:main]}})

(defn init-db-fx
  [db _]
  (merge default-db db))

(defn ipc-initialised-db-fx
  [db _]
  (assoc db :ipc-initialised? true))

(defn dispatch-event
  [event]
  (re-frame/dispatch [(:type event) event]))

(defn register!
  []
  (re-frame/reg-event-db
   ::initialize-db
   init-db-fx)

  (re-frame/reg-event-fx
   :config-read-request-event
   config-fx/config-read-request-fx)

  (re-frame/reg-event-fx
   :config-read-event
   config-fx/config-read-fx)

  (re-frame/reg-event-fx
   :config-read-failure-event
   config-fx/config-read-failure-fx)

  (re-frame/reg-event-fx
   :config-save-request-event
   config-fx/config-save-request-fx)

  (re-frame/reg-event-fx
   :config-save-success-event
   config-fx/config-save-success-fx)

  (re-frame/reg-event-fx
   :config-save-failure-event
   config-fx/config-save-failure-fx)

  (re-frame/reg-event-fx
   :config-reset-request-event
   config-fx/config-reset-request-fx)

  (re-frame/reg-event-fx
   :config-value-change-event
   config-fx/config-value-change-fx)

  (re-frame/reg-event-fx
   :config-value-clear-event
   config-fx/config-value-clear-fx)

  (re-frame/reg-event-fx
   :dataset-add-event
   config-fx/dataset-add-fx)

  (re-frame/reg-event-fx
   :dataset-remove-event
   config-fx/dataset-remove-fx)

  (re-frame/reg-event-fx
   :process-state-change-request-event
   process-fx/process-command-event-fx)

  (re-frame/reg-event-fx
   :process-state-change-event
   process-fx/process-state-change-fx)

  (re-frame/reg-event-fx
   :process-state-notification-event
   process-fx/process-state-notification-fx)

  (re-frame/reg-event-fx
   :process-state-change-failure-event
   process-fx/process-state-change-failure-fx)

  (re-frame/reg-event-fx
   :process-state-notification-request-event
   process-fx/process-state-notification-request-fx)

  (re-frame/reg-event-fx
   :process-runtime-info-notification-request-event
   process-fx/process-runtime-info-notification-request-fx)

  (re-frame/reg-event-fx
   :process-runtime-info-notification-event
   process-fx/process-runtime-info-notification-fx)

  (re-frame/reg-event-fx
   :navigation-route-change-event
   navigation-fx/navigation-route-change-fx)

  (re-frame/reg-event-fx
   :logs-read-request-event
   logs-fx/logs-read-request-fx)

  (re-frame/reg-event-db
   :logs-read-success-event
   logs-fx/logs-read-success-db-fx)

  (re-frame/reg-event-fx
   :logs-read-failure-event
   logs-fx/logs-read-failure-fx)

  (re-frame/reg-event-db
   :ipc-initialised
   ipc-initialised-db-fx)

  (ipc/register-listener
   :main
   dispatch-event))
