(ns camelot.launcher.renderer.subs.core
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::route
 (fn [db _]
   (get-in db [:navigation :route])))

(re-frame/reg-sub
 ::ipc-initialised?
 (fn [db _]
   (get db :ipc-initialised?)))
