(ns camelot.launcher.logs.core
  "Log file management for Camelot."
  (:require [camelot.launcher.logs.event :as logs-event]
            [camelot.launcher.config.core :as config]
            [camelot.launcher.event.core :as event]
            [camelot.launcher.common.log :as log]
            [clojure.string :as cstr]
            [cljs.core.async :as async :include-macros true]
            [integrant.core :as ig]))

(def module-key :logs)
(def module (hash-map module-key {:logs/event (ig/ref event/module-key)
                                  :logs/config (ig/ref config/module-key)}))

(defonce fs (js/require "fs"))
(defonce path (js/require "path"))

(defn- get-config
  [system]
  @(-> system :logs/config :config))

(def log-file "camelot.log")

(defn- get-log-path
  [system]
  (.join path
         (-> system get-config :paths :log)
         log-file))

(defn process
  [s]
  (-> s
      (cstr/replace #"\[\d+m" "")
      (cstr/replace #"\[(\d{4}-\d{2}-\d{2} \d+\:\d+\:\d+,\d+)\]\[(.*?)\]\[(.*?)\]" "\n$1 from $3\n$2: ")))

(defn handle-log-read
  [system error contents]
  (async/put! (::broadcast-channel-tx system)
              (if error
                (logs-event/logs-read-success (log/session-history)
                                              (ex-message error))
                (logs-event/logs-read-success (log/session-history)
                                              contents))))

(defn handle-event
  [system event]
  (condp = (:type event)
    :logs-read-request-event
    (.readFile fs (get-log-path system)
               #js {:encoding "utf8"}
               (partial handle-log-read system))
    nil))

(defn- start-listener!
  [system]
  (let [ch (::broadcast-channel-rx system)]
    (async/go-loop []
      (let [event (async/<! ch)]
        (handle-event system event))
      (recur))
    ch))

(defmethod ig/prep-key module-key [_ system]
  (assoc system
         ::broadcast-channel-tx (async/chan)))

(defmethod ig/init-key module-key [_ system]
  (let [broadcast-ch-rx ((-> system :logs/event :register!)
                         (::broadcast-channel-tx system))
        s (assoc system ::broadcast-channel-rx broadcast-ch-rx)]
    (start-listener! s)
    s))
