(ns camelot.launcher.ui.action.status
  "Curated information around the current application status."
  (:require [goog.object :as obj]
            [goog.string :as gstr]
            [clojure.string :as cstr]
            goog.string.format))

(defn- get-config
  [system ks]
  (get-in @(-> system :ui/config :config) ks))

(defn- current-status
  [system]
  ((-> system :ui/process :status)))

(defn- java-configuration
  [system]
  ((-> system :ui/environ :java-configuration)))

(defn- find-flag-configuration
  [output flag process]
  (some->> output
           cstr/split-lines
           (filter #(re-find (re-pattern (str "\\s" flag "\\s")) %))
           first
           (re-find #"=\s*([^ \t]+)")
           second
           process))

(defn- java-details
  [system]
  (let [result (java-configuration system)]
    (if (zero? (obj/get result "status"))
      {:max-heap-size (find-flag-configuration (obj/get result "stdout") "MaxHeapSize" int)
       :version (.toString (obj/get result "stderr"))}
      {:error (or (obj/get result "error")
                  (obj/get result "stderr")
                  "Failed to get Java details. Please check your java command and JVM arguments.")})))

(defn summary
  [system]
  (let [status (current-status system)
        version (:software-version status)]
    (let [process-state (:process-state status)]
      (cond-> {:jvm (java-details system)
               :status (select-keys status [:process-state :process-state-description])}
        (and version (= process-state :started))
        (assoc :process (assoc (select-keys status [:software-version :database-version])
                               :http-port (get-config system [:server :http-port])))))))

(def summary-str-template
  "Status: %s

Max available memory: %s

Java version:
%s

Camelot version: %s
Database version: %s
HTTP port: %s
")

(defn summary-str
  [system]
  (let [s (summary system)]
    (gstr/format summary-str-template
                 (-> s :status :process-state-description)
                 (if-let [heap (-> s :jvm :max-heap-size)]
                   (str (.toPrecision (/ heap 1024 1024 1024) 3) " GB")
                   "N/A")
                 (-> s :jvm :version)
                 (or (-> s :process :software-version) "N/A")
                 (or (-> s :process :database-version) "N/A")
                 (if (:process s)
                   (get-config system [:server :http-port])
                   "N/A"))))
