(ns camelot.launcher.process.event)

(defn server-state-change
  [{:keys [new-state old-state]}]
  {:type :process-state-change-event
   :new-state new-state
   :old-state old-state})

(defn server-state-change-failure
  [error]
  {:type :process-state-change-failure-event
   :error {:type :process-error
           :message error}})

(defn server-state-notification
  [current-state]
  {:type :process-state-notification-event
   :current-state current-state})

(defn server-runtime-info
  [info]
  (assoc info
         :type :process-runtime-info-notification-event))
