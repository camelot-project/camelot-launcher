(ns camelot.launcher.process.healthcheck
  (:refer-clojure :exclude [apply])
  (:require [camelot.launcher.process.status :as status]
            [camelot.launcher.process.event :as process-event]
            [cljs.core.async :as async :include-macros true]
            [httpurr.client.node :as http]
            [promesa.core :as p]
            [goog.string :as gstr]
            goog.string.format))

(defn- request
  [config opts]
  (http/get (gstr/format "http://localhost:%d/heartbeat"
                         (get-in config [:server :http-port]))
              opts))

(defn- heartbeat
  [system publish!]
  (let [config @(-> system :process/config :config)]
    (try
      (-> (request config {:timeout 2000})
          (p/then publish!)
          (p/catch (fn [e] (publish! {}))))
      (catch js/Error e
        (publish! {})))))

(defn- is-healthy?
  [status body]
  (and (= status 200)
       (= (get body "status") "OK")))

(defn- parser
  [system response]
  (let [status (:status response)
        body (js->clj (.parse js/JSON (:body response)))]
    (if (is-healthy? status body)
      {:healthy? true
       :software-version (get body "software-version")
       :database-version (get body "database-version")}
      {:healthy? false})))

(defn event-handler
  [system broadcast-tx event]
  (condp = (:type event)
    ::healthcheck
    (do
      (let [result (:result event)
            change (status/apply-healthcheck! system result)]
        (if change
          [[broadcast-tx
            (process-event/server-state-change change)]]
          [])))
    nil))

(defn apply
  [system publish!]
  (heartbeat system #(-> (parser system %) publish!)))

(defn start-listener!
  [system broadcast-tx]
  (let [ch (async/chan)]
    (apply system #(async/put! ch {:type ::healthcheck :result %}))
    (async/go-loop []
      (let [timeout-ch (async/timeout 2000)
            [event port] (async/alts! [ch timeout-ch])]
        (if (= port timeout-ch)
          (apply system #(async/put! ch {:type ::healthcheck :result %}))
          (doseq [[c evt] (event-handler system broadcast-tx event)]
            (async/>! c evt))))
      (recur))))
