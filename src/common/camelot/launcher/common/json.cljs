(ns camelot.launcher.common.json)

;; TODO support camelot-market from JSON conversion
(def paths-to-keyword-values
  [[:type]
   [:target-state]
   [:current-state]
   [:new-state]
   [:old-state]
   [:error :type]])

(defn- keywordify-values
  [c]
  (reduce
   (fn [acc path]
     (if (get-in acc path)
       (update-in acc path keyword)
       acc))
   c paths-to-keyword-values))

(defn from-json
  [o]
  (-> o
      (js->clj :keywordize-keys true)
      (keywordify-values)))

(def to-json clj->js)
