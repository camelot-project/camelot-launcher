(ns camelot.launcher.common.events
  (:require [clojure.spec.alpha :as s]
            [camelot.market.spec :as market-s]))

(defn config-save-request
  [configuration]
  {:type :config-save-request-event
   :configuration configuration})

(defn config-read-request
  []
  {:type :config-read-request-event})

(defn config-reset-request
  []
  {:type :config-reset-request-event})

(defn logs-read-request
  []
  {:type :logs-read-request-event})

(defn server-state-change-request
  [target-state]
  {:type :process-state-change-request-event
   :target-state target-state})

(defn server-state-notification-request
  []
  {:type :process-state-notification-request-event})

(defn server-runtime-info-notification-request
  []
  {:type :process-runtime-info-notification-request-event})

(defn launcher-quit
  []
  {:type :launcher-quit-event})

(def valid-state?
  #{:stopping
    :stopped
    :starting
    :started
    :unknown})

(s/def ::new-state valid-state?)
(s/def ::old-state valid-state?)
(s/def ::current-state valid-state?)
(s/def ::target-state #{:stopped :started})

;; TODO would be nice to flesh this out at some point.
(s/def ::type keyword?)
(s/def ::message string?)
(s/def ::error (s/keys :req-un [::type ::message]))

(s/def :process/alternate-urls (s/coll-of string? :distinct true))
(s/def :process/software-version string?)
(s/def :process/database-version string?)

(s/def :log/session string?)
(s/def :log/process string?)
(s/def :log/contents (s/keys :req-un [:log/session :log/process]))

(s/def :config/problem-path (s/coll-of keyword? :kind vector?))
(s/def :config/problems (s/coll-of :config/problem-path :kind set?))
(s/def ::configuration ::market-s/config)

(defmulti event-type :type)

(defmethod event-type :process-state-change-request-event [_]
  (s/keys :req-un [::type ::target-state]))

(defmethod event-type :config-save-request-event [_]
  (s/keys :req-un [::type]))

(defmethod event-type :config-reset-request-event [_]
  (s/keys :req-un [::type]))

(defmethod event-type :config-save-success-event [_]
  (s/keys :req-un [::type ::configuration]))

(defmethod event-type :config-save-failure-event [_]
  (s/keys :req-un [::type :config/problems]))

(defmethod event-type :config-read-failure-event [_]
  (s/keys :req-un [::type ::error]))

(defmethod event-type :process-state-change-event [_]
  (s/keys :req-un [::type ::new-state ::old-state]))

(defmethod event-type :config-read-request-event [_]
  (s/keys :req-un [::type]))

(defmethod event-type :config-read-event [_]
  (s/keys :req-un [::type ::configuration]))

(defmethod event-type :process-state-change-failure-event [_]
  (s/keys :req-un [::type ::error]))

(defmethod event-type :process-state-notification-request-event [_]
  (s/keys :req-un [::type]))

(defmethod event-type :process-state-notification-event [_]
  (s/keys :req-un [::type]))

(defmethod event-type :process-runtime-info-notification-request-event [_]
  (s/keys :req-un [::type]))

(defmethod event-type :process-runtime-info-notification-event [_]
  (s/keys :req-un [::type
                   :process/alternate-urls
                   :process/software-version
                   :process/database-version]))

(defmethod event-type :logs-read-request-event [_]
  (s/keys :req-un [::type]))

(defmethod event-type :logs-read-success-event [_]
  (s/keys :req-un [::type :log/contents]))

(defmethod event-type :logs-read-failure-event [_]
  (s/keys :req-un [::type ::error]))

(defmethod event-type :launcher-quit-event [_]
  (s/keys :req-un [::type]))

(s/def ::event (s/multi-spec event-type ::type))
