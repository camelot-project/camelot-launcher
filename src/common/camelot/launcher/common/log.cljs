(ns camelot.launcher.common.log
  (:require [clojure.string :as cstr]))

(def session-history-atom (atom []))

(def session-history-scrollback 1000)

(defn- redact-path
  [event path]
  (if (get-in event path)
    (assoc-in event path "<redacted>")
    event))

(defn- redact-event
  [event]
  (-> event
      (redact-path [:configuration :detector :username])
      (redact-path [:configuration :detector :password])
      (redact-path [:configuration :client-id])))

(defn- bounded-conj
  [xs x]
  (let [size (count xs)
        discard (max (- size session-history-scrollback) 0)]
    (conj (vec (drop discard xs)) x)))

(defn- process-log-message
  [msg]
  (if (map? msg)
    (redact-event msg)
    msg))

(defn debug?
  []
  (.-DEBUG js/goog))

(defn log
  [level & s]
  (when (or (not= level :debug) (debug?))
    (let [s (map process-log-message s)]
      (swap! session-history-atom bounded-conj (cstr/join " " s))
      (apply println s))))

(defn session-history
  []
  (cstr/join "\n"
             (mapv #(if (>= (count %) 2000)
                     (str (subs % 0 400) "...")
                     %)
                  @session-history-atom)))
