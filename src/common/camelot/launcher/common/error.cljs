(ns camelot.launcher.common.error)

(def chain-max-unroll-depth 10)

(defn stringify-stack
  [e]
  (or (.-stack e)
      (str (.-name e) (.-message e) "\n"
           "    <no stack>")))

(declare stringify-error)

(defn- stringify-cause
  [acc e rem-depth]
  (if-let [cause (and (pos-int? rem-depth) (.-cause e))]
    (fn []
      (stringify-error (str acc "\n" "Caused by " ) cause (dec rem-depth)))
    acc))

(defn- stringify-error
  ([e]
   (stringify-error "" e chain-max-unroll-depth))
  ([acc e rem-depth]
   (if (zero? rem-depth)
     (str acc "<limit reached>")
     (stringify-cause (str acc (stringify-stack e)) e rem-depth))))

(defn stringify-error-chain
  [e]
  (trampoline stringify-error e))
