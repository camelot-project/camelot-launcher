(ns camelot.launcher.ui.action.status-test
  (:require [camelot.launcher.ui.action.status :as sut]
            [cljs.test :refer [deftest testing is] :include-macros true]))

(def ^:private java-configuration
  (constantly #js {"stdout" " MaxHeapSize  =   1000000000"
                   "status" 0
                   "stderr" (reify Object
                              (toString [this] "Version 11.0.0"))}))

(def ^:private status
  (constantly {:software-version "1.4.5"
               :database-version "040"
               :process-state :started
               :process-state-description "Started"}))

(def ^:private default-system
  {:ui/config {:config (atom {:server {:http-port 8088}})}
   :ui/process {:status status}
   :ui/environ {:java-configuration java-configuration}})

(deftest summary-test
  (testing "summary"
    (testing "should return the expected data when the system is running"
      (let [expected {:jvm {:max-heap-size 1000000000 :version "Version 11.0.0"}
                      :status {:process-state :started
                               :process-state-description "Started"}
                      :process {:software-version "1.4.5"
                                :database-version "040"
                                :http-port 8088}}]
        (is (= (sut/summary default-system) expected))))

    (testing "should omit process information if not started"
      (let [system (assoc-in default-system
                             [:ui/process :status]
                             (constantly {:software-version "1.4.5"
                                          :process-state :stopping}))
            expected {:jvm {:max-heap-size 1000000000 :version "Version 11.0.0"}
                      :status {:process-state :stopping}}]
        (is (= (sut/summary system) expected))))

    (testing "should omit process information if application version is not known"
      (let [system (assoc-in default-system [:ui/process :status]
                             (constantly {:process-state :started}))
            expected {:jvm {:max-heap-size 1000000000 :version "Version 11.0.0"}
                      :status {:process-state :started}}]
        (is (= (sut/summary system) expected))))))

(deftest summary-str-test
  (testing "summary-str"
    (testing "should return the expected data when the system is not running"
      (let [expected "Status: Started

Max available memory: 0.931 GB

Java version:
Version 11.0.0

Camelot version: 1.4.5
Database version: 040
HTTP port: 8088
"]
        (is (= (sut/summary-str default-system) expected))))

    (testing "should omit process information if not started"
      (let [system (assoc-in default-system
                             [:ui/process :status]
                             (constantly {:software-version "1.4.5"
                                          :process-state :stopping
                                          :process-state-description "Stopping"}))
            expected "Status: Stopping

Max available memory: 0.931 GB

Java version:
Version 11.0.0

Camelot version: N/A
Database version: N/A
HTTP port: N/A
"]
        (is (= (sut/summary-str system) expected))))

    (testing "should omit process information if application version is not known"
      (let [system (assoc-in default-system [:ui/process :status]
                             (constantly {:process-state :started
                                          :process-state-description "Started"}))
            expected "Status: Started

Max available memory: 0.931 GB

Java version:
Version 11.0.0

Camelot version: N/A
Database version: N/A
HTTP port: N/A
"]
        (is (= (sut/summary-str system) expected))))))
